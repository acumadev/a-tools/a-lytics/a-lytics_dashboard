import { LoginService } from '../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  constructor(
    private router: Router,
    private login: LoginService
  ) { }

  ngOnInit(): void {
  }

  loginAccount(username, password){
    let data = { username: username.value, password: password.value }
    this.login.loginAccount(data)
    .subscribe((data:any) => {
      localStorage.setItem('alytics-hash-user', JSON.stringify(data.data))
      this.router.navigate(['/master']);
    })
    password.value = ""
  }

}
