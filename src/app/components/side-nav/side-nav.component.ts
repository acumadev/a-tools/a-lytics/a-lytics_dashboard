import { Component, OnInit } from '@angular/core';
import { WebsitesService } from '../../services/websites.service';
@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {

  websites = []
  constructor(private website: WebsitesService) { }

  ngOnInit(): void {
    this.website.getWebsites()
    .subscribe((data:any) =>{
      this.websites = data.data
    })
  }

}
