import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullAnalyticsViewComponent } from './full-analytics-view.component';

describe('FullAnalyticsViewComponent', () => {
  let component: FullAnalyticsViewComponent;
  let fixture: ComponentFixture<FullAnalyticsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FullAnalyticsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullAnalyticsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
