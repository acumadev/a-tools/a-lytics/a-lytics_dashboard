import { AnalyticsService } from "../../services/analytics.service";
import { WebsitesService } from '../../services/websites.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-full-analytics-view',
  templateUrl: './full-analytics-view.component.html',
  styleUrls: ['./full-analytics-view.component.scss']
})
export class FullAnalyticsViewComponent implements OnInit {

  website_info
  buttons = []
  inputs = []
  links = []
  constructor(
    private analytics: AnalyticsService,
    private website: WebsitesService,
    private route: ActivatedRoute,
    public location: Location
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.website.getWebsite(params.get('website_id'))
      .subscribe((data:any) =>{
        this.website_info = data.data
      })
      this.analytics.getElementsAnalytics(params.get('website_id'))
      .subscribe((data:any) =>{
        for(let el of data.data){
          if(el.type == "BUTTON"){ this.buttons.push(el) }
          else if(el.type == "INPUT"){ this.inputs.push(el)}
          else if(el.type == "A"){ this.links.push(el) }
        }
      })
    });
  }
}
