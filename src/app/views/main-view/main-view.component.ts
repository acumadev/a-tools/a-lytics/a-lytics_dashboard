import { WebsitesService } from '../../services/websites.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.scss']
})
export class MainViewComponent implements OnInit {

  websites = []
  constructor(
    private sanitizer: DomSanitizer,
    private website: WebsitesService
  ) { }

  ngOnInit(): void {
    this.website.getWebsites()
    .subscribe((data:any) =>{
      this.websites = data.data
    })
  }

  sanitizeMe(url: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
