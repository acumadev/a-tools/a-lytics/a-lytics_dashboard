import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import { Component, OnInit, Input } from '@angular/core';
import { Color, Label } from 'ng2-charts';
@Component({
  selector: 'app-months-views-chart',
  templateUrl: './months-views-chart.component.html',
  styleUrls: ['./months-views-chart.component.scss']
})
export class MonthsViewsChartComponent implements OnInit {
  @Input() chart_all_data

  public lineChartData: ChartDataSets[] = [
    { data: [], label: 'Visitas' },
  ];
  public lineChartLabels: Label[] = [];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      yAxes: [
          {
              ticks: {
                  min: 0, // it is for ignoring negative step.
                  beginAtZero: true,
                  stepSize: 1  // if i use this it always set it '1', which look very awkward if it have high value  e.g. '100'.
              }
          }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'Marzo',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };

  public lineChartColors: Color[] = [
    { // grey
      backgroundColor: '#576CA8',
      borderColor: '#576CA8',
      pointBackgroundColor: '#F5F3F5',
      pointBorderColor: '#F5F3F5',
      pointHoverBackgroundColor: '#F5F3F5',
      pointHoverBorderColor: '#F5F3F5'
    },
  ];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [pluginAnnotations];
  constructor( 
  ) { }

  ngOnInit() {
    console.log(this.chart_all_data)
    for(let month of this.chart_all_data){
      this.lineChartData[0].data.push(month.visits)
      this.lineChartLabels.push(this.getMonth(month.month))
    }
    this.lineChartData[0].data.push(0)
    this.lineChartLabels.push("Mes Próximo")
  }

  getMonth(month) {
    switch(month){
      case 1: return "Enero"
      case 2: return "Febrero"
      case 3: return "Marzo"
      case 4: return "Abril"
      case 5: return "Mayo"
      case 6: return "Junio"
      case 7: return "Julio"
      case 8: return "Agosto"
      case 9: return "Septiembre"
      case 10: return "Octubre"
      case 11: return "Noviembre"
      case 12: return "Diciembre"
    }  
  }
  chartClicked(e){
    alert(e)
    console.log(e)
  }
}
