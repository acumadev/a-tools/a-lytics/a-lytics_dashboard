import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthsViewsChartComponent } from './months-views-chart.component';

describe('MonthsViewsChartComponent', () => {
  let component: MonthsViewsChartComponent;
  let fixture: ComponentFixture<MonthsViewsChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthsViewsChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthsViewsChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
