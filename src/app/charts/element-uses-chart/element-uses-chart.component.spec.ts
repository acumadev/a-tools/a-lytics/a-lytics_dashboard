import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementUsesChartComponent } from './element-uses-chart.component';

describe('ElementUsesChartComponent', () => {
  let component: ElementUsesChartComponent;
  let fixture: ComponentFixture<ElementUsesChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementUsesChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementUsesChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
