import { AnalyticsService } from "../../services/analytics.service";
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Color, Label } from 'ng2-charts';
@Component({
  selector: 'app-element-uses-chart',
  templateUrl: './element-uses-chart.component.html',
  styleUrls: ['./element-uses-chart.component.scss']
})
export class ElementUsesChartComponent implements OnInit {

  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [
          {
              ticks: {
                  min: 0, // it is for ignoring negative step.
                  beginAtZero: true,
                  stepSize: 1  // if i use this it always set it '1', which look very awkward if it have high value  e.g. '100'.
              }
          }
      ]
    },
  };
  public barChartLabels: Label[] = ['Usos Totales'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];
  public lineChartColors: Color[] = [
    {
      backgroundColor: '#1B264F',
      borderColor: '#1B264F',
    },
    {
      backgroundColor: '#274690',
      borderColor: '#274690',
    },
    {
      backgroundColor: '#576CA8',
      borderColor: '#576CA8',
    }
  ];
  public barChartData: ChartDataSets[] = [];

  constructor(
    private analytics: AnalyticsService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.analytics.getElementsAnalytics(params.get('website_id'))
      .subscribe((datas:any) =>{
        let buttons = 0; let inputs = 0; let links = 0
        for(let el of datas.data){
          if(el.type == "BUTTON"){ buttons = buttons + el.uses }
          else if(el.type == "INPUT"){ inputs = inputs + el.uses }
          else if(el.type == "A"){ links = links + el.uses }
        }
        this.barChartData.push(
          {data:[buttons], label:"Botones"},
          {data:[inputs], label:"Entradas de Texto"},
          {data:[links], label:"Enlaces"}
        )
      })
    });
  }

}
