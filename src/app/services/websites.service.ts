import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WebsitesService {
  url = environment.alytics_url

  constructor(private http: HttpClient) { }

  getWebsites() {
    let user
    if(environment.production){
      user = localStorage.getItem('alytics-hash-user')
    }else{
      user = environment.test_user
    }
    return this.http.get(this.url+'/'+user+'/websites')
      .pipe(map(websites => {
      return websites;
    }));
  }

  getWebsite(website_id) {
    let user
    if(environment.production){
      user = localStorage.getItem('alytics-hash-user')
    }else{
      user = environment.test_user
    }
    return this.http.get(this.url+'/'+user+'/websites/'+website_id)
      .pipe(map(website => {
      return website;
    }));
  }
}
