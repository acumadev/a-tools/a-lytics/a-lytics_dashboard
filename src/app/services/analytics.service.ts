import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";

import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {
  url = environment.alytics_url

  constructor(private http: HttpClient) { }

  getVisitsAnalytics(website_id) {
    let user
    if(environment.production){
      user = localStorage.getItem('alytics-hash-user')
    }else{
      user = environment.test_user
    }
    return this.http.get(this.url+'/'+user+'/websites/'+website_id+'/visits')
      .pipe(map(visits => {
      return visits;
    }));
  }

  getElementsAnalytics(website_id) {
    let user
    if(environment.production){
      user = localStorage.getItem('alytics-hash-user')
    }else{
      user = environment.test_user
    }
    return this.http.get(this.url+'/'+user+'/websites/'+website_id+'/elements')
      .pipe(map(elements => {
      return elements;
    }));
  }
}
