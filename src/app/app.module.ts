import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

'Material Angular'
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatTreeModule } from '@angular/material/tree';
import { MatCardModule } from '@angular/material/card';

'Charts JS'
import { ChartsModule } from 'ng2-charts';

'Custom Components'
import { MainViewComponent } from './views/main-view/main-view.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { FullAnalyticsViewComponent } from './views/full-analytics-view/full-analytics-view.component';

'Charts '
import { MonthsViewsChartComponent } from './charts/months-views-chart/months-views-chart.component';
import { ElementUsesChartComponent } from './charts/element-uses-chart/element-uses-chart.component';
import { LoginViewComponent } from './views/login-view/login-view.component';
import { LoginFormComponent } from './components/login-form/login-form.component';

@NgModule({
  declarations: [
    AppComponent,
    MainViewComponent,
    SideNavComponent,
    FullAnalyticsViewComponent,
    MonthsViewsChartComponent,
    ElementUsesChartComponent,
    LoginViewComponent,
    LoginFormComponent,
  ],
  imports: [
    ChartsModule,
    MatTreeModule,
    BrowserModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    HttpClientModule,
    MatSidenavModule,
    MatToolbarModule,
    AppRoutingModule,
    MatFormFieldModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }