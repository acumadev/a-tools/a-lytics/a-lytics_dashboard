import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoutesGuard } from './guards/routes.guard'; 

import { MainViewComponent } from './views/main-view/main-view.component';
import { LoginViewComponent } from './views/login-view/login-view.component';
import { FullAnalyticsViewComponent } from './views/full-analytics-view/full-analytics-view.component';

const routes: Routes = [
  { path: 'master/:website_id/full-analytics', component: FullAnalyticsViewComponent, canActivate: [RoutesGuard] },
  { path: 'master', component: MainViewComponent, canActivate: [RoutesGuard] },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginViewComponent },
  { path: '**', redirectTo: '/login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
